import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardGuard} from './guards/auth-guard.guard';
import {ItemsResolveService} from './shared/items-resolve.service';

const routes: Routes = [
  {
    path: 'item-list',
    loadChildren: 'app/item-list/item-list.module#ItemListModule',
    canActivate: [AuthGuardGuard],
    resolve: {
      itemsList: ItemsResolveService
    }
  },
  {
    path: 'checkbox-items',
    loadChildren: 'app/checkbox-items/checkbox-items.module#CheckboxItemsModule'
  },
  {
    path: 'login',
    loadChildren: 'app/login/login.module#LoginModule'
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
