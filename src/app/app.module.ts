import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import {AuthGuardGuard} from './guards/auth-guard.guard';
import {ItemsResolveService} from './shared/items-resolve.service';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [AuthGuardGuard, ItemsResolveService],
  bootstrap: [AppComponent]
})
export class AppModule { }
