import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';

@Injectable()
export class ItemsResolveService implements Resolve<any> {

  constructor() { }

  resolve(route: ActivatedRouteSnapshot) {
    return Observable.fromPromise(fetch('https://randomuser.me/api/?results=20')
      .then(data => data.json())
      .then(parsedData => parsedData.results)
      .catch(err => console.log(err)));
  }
}
