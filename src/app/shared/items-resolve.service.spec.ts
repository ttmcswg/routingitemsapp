import { TestBed, inject } from '@angular/core/testing';

import { ItemsResolveService } from './items-resolve.service';

describe('ItemsResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItemsResolveService]
    });
  });

  it('should be created', inject([ItemsResolveService], (service: ItemsResolveService) => {
    expect(service).toBeTruthy();
  }));
});
