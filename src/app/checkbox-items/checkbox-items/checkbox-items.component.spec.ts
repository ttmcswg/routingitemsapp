import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxItemsComponent } from './checkbox-items.component';

describe('CheckboxItemsComponent', () => {
  let component: CheckboxItemsComponent;
  let fixture: ComponentFixture<CheckboxItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckboxItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
