import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CheckboxItemsRoutingModule } from './checkbox-items-routing.module';
import { CheckboxItemsComponent } from './checkbox-items/checkbox-items.component';

@NgModule({
  imports: [
    CommonModule,
    CheckboxItemsRoutingModule
  ],
  declarations: [CheckboxItemsComponent]
})
export class CheckboxItemsModule { }
