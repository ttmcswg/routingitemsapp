import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CheckboxItemsComponent} from './checkbox-items/checkbox-items.component';

const routes: Routes = [
  {
    path: '',
    component: CheckboxItemsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckboxItemsRoutingModule { }
