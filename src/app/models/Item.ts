export interface Item {
  title: string;
  amount: number;
  checked: boolean;
  id?: number;
}
