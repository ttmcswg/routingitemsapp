import { Component } from '@angular/core';
import {User} from '../../models/User';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent {
  private user: User = {
    login: '',
    password: '',
    role: ''
  };

  private usersMock: Array<User> = [
    {
      login: 'admin',
      password: 'admin',
      role: 'admin'
    },
    {
      login: 'moder',
      password: 'moder',
      role: 'moder'
    }
  ];

  handleSubmit() {
    this.usersMock.forEach(user => {
      if (user.login === this.user.login && user.password === this.user.password) {
        sessionStorage.setItem('current_user', JSON.stringify(this.user.login));
        alert(`Hello ${this.user.login}`);
      }
    });
  }

  constructor() { }

}
